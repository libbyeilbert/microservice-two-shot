import React, { useEffect, useState } from 'react';

function HatForm ({fetchHats}) {
    const [fabric, setFabric] = useState('');
    const [style_name, setStyleName] = useState('');
    const [color, setColor] = useState('');
    const [picture_url, setPictureUrl] = useState('');
    const [location, setLocation] = useState('');
    const [locations, setLocations] = useState([]);

    const handleFabricChange = (event) => {
        setFabric(event.target.value);
    };

    const handleStyleNameChange = (event) => {
        setStyleName(event.target.value);
    };

    const handleColorChange = (event) => {
        setColor(event.target.value);
    };

    const handlePictureUrlChange = (event) => {
        setPictureUrl(event.target.value);
    };

    const handleLocationChange = (event) => {
        setLocation(event.target.value);
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {
            fabric: fabric,
            style_name: style_name,
            color: color,
            picture_url: picture_url,
            location: location
        };

        console.log(data)
        // {     make sure to format the requests like this 
        //     "fabric": "felt",
        //     "style_name": "pimp hat",
        //     "color": "pink",
        //     "picture_url": "https://i.imgur.com/xuyWIIp.jpeg",
        //     "location": "/api/locations/1/"
        //   }
          


        const hatUrl = 'http://localhost:8090/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type':'application/json',
            },
        };
        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat);
            fetchHats();
            setFabric('');
            setStyleName('');
            setColor('');
            setPictureUrl('');
            setLocation('');
        }
    };

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new hat</h1>
                    <form onSubmit={handleSubmit} id="create-hat-form">
                        <label htmlFor="fabric">Fabric</label>
                        <div className="form-floating mb-3">
                            <input value={fabric} onChange={handleFabricChange} placeholder="Fabric" type="text" name="fabric" id="fabric" />
                        </div>
                        <label htmlFor="style_name">Style Name</label>
                        <div className="form-floating mb-3">
                            <input value={style_name} onChange={handleStyleNameChange} placeholder="Style Name" type="text" name="style_name" id="style_name" />
                        </div>
                        <label htmlFor="color">Color</label>
                        <div className="form-floating mb-3">
                            <input value={color} onChange={handleColorChange} placeholder="Color" type="text" name="color" id="color" />
                        </div>
                        <label htmlFor="picture_url">Picture</label>
                        <div className="form-floating mb-3">
                            <input value={picture_url} onChange={handlePictureUrlChange}  placeholder="Picture" type="text" name="picture_url" id="picture_url" />
                        </div>
                        <div className="mb-3">
                            <select value={location} onChange={handleLocationChange} required name="location" id="location" className="form-select">
                                <option value="">Choose a location</option>
                                {locations.map(location => {
                                    return (
                                        <option key={location.href} value={location.href}>
                                            {location.closet_name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default HatForm;



