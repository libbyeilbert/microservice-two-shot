from django.db import models


class LocationVO(models.Model):
    closet_name = models.CharField(max_length=200)
    import_href = models.CharField(max_length=200)

class Hat(models.Model):
    fabric = models.CharField(max_length=200)
    style_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture_url = models.CharField(max_length=200)
    location = models.ForeignKey(LocationVO, on_delete=models.CASCADE)


# to do list: go through the hatform.js and make sure all references to location and locations(array)
# make sure that hatlist.js is properly formatted and not incorperating the same floating description labels problem 
# finish/fix the apps.js file to include paths to hatform AND hatlist