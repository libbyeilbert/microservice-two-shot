from django.contrib import admin
from .models import Location, Bin

admin.site.register(Location)
admin.site.register(Bin)
