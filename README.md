# Wardrobify

Team:

* Person 1 - Libby Eilbert (shoes)
* Person 2 - Dana Blanchard (hats)

## Design

## Shoes microservice

Create a Shoe model and a view function to list all shoes. In order to add the shoe's bin as a foreign key attribute, create a Bin value object and a poller that gets Bin data from the wardrope API. Add a POST method to the view function to create a shoe. Write a React form to create a new shoe and add a link to it in the navigation bar. Add a function to the shoes list to delete a shoe (and refresh the page on success) and call it in an onClick handler for each shoe in the list.

## Hats microservice

We started with models, then created our views, and after configuring the admin, urls, nav page, etc, we created our react forms to render it all. We succesfully implemented post, get, and delete in insomnia. Then I went out of my way to find really cute pictures to of toads in hats to test out my components.
