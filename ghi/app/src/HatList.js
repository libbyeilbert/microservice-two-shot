import { useState } from 'react';

function HatList(props) {
  const [loading, setLoading] = useState(false);

//   const handleDeleteHat = async (hatHref) => {
//     setLoading(true);
//     try {
//       // Perform the DELETE request to the backend API
//       const response = await fetch(`http://localhost:8090/api/hats/${hatHref}/`, {
//         method: 'DELETE',
//         headers: {
//           'Content-Type': 'application/json',
//         },
//       });

//       if (response.ok) {
//         // If the delete request was successful, remove the hat from the list
//         const updatedHats = props.hats.filter((hat) => hat.href !== hatHref);
//         props.setHats(updatedHats); // Assuming you have a `setHats` function to update the hats state
//       } else {
//         // Handle the case where the delete request was not successful
//         console.error('Failed to delete hat');
//       }
//     } catch (error) {
//       console.error('Error while deleting hat:', error);
//     } finally {
//       setLoading(false);
//     }
//   };

  if (props.hats === undefined) {
    return null;
  }

  const handleDeleteHat = (HatId) => {
    const hatUrl = `http://localhost:8090/hats/${HatId}/`
    console.log(hatUrl);

    fetch( hatUrl, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application.json',
        },
    }
    )
    .then(response => {
        if (response.ok) {
            console.log("Hat deleted.");
            window.location.reload();
        }
    })
}

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Fabric</th>
          <th>Style Name</th>
          <th>Color</th>
          <th>Picture URL</th>
          <th>Location (Closet Name)</th>
          <th>Delete Hat</th>
        </tr>
      </thead>
      <tbody>
        {props.hats.map(hat => {
          return (
            <tr key={hat.id}>
              <td>{hat.fabric}</td>
              <td>{hat.style_name}</td>
              <td>{hat.color}</td>
              <td><a href={hat.picture_url}>Link</a></td>
              <td>{hat.location.closet_name}</td>
              <td>
                <button onClick={() => handleDeleteHat(hat.id)} className="btn btn-danger">Delete</button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default HatList;