from django.contrib import admin
from shoes_rest.models import Shoe, BinVO

@admin.register(Shoe)
class ShoeAdmin(admin.ModelAdmin):
    list_display = (
        "color",
    )

@admin.register(BinVO)
class BinVOAdmin(admin.ModelAdmin):
    list_display = (
        "bin_number",
    )
