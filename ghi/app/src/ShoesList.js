

function ShoesList(props) {
    if (props.shoes === undefined) {
        return null;
      }
    const handleDelete = (shoeId) => {
        const shoeUrl = `http://localhost:8080/api/shoes/${shoeId}/`
        console.log(shoeUrl);

        fetch( shoeUrl, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application.json',
            },
        }
        )
        .then(response => {
            if (response.ok) {
                console.log("Shoe deleted.");
                window.location.reload();
            }
        })
    }
    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Manufacturer</th>
                    <th>Model Name</th>
                    <th>Color</th>
                    <th>Bin (closet name, bin number)</th>
                    <th>Delete Shoe</th>
                </tr>
            </thead>
            <tbody>
                {props.shoes.map(shoe => {
                    return (
                        <tr key={shoe.id}>
                            <td>{ shoe.manufacturer }</td>
                            <td>{ shoe.model_name }</td>
                            <td>{ shoe.color }</td>
                            <td>{ shoe.bin.closet_name }, bin { shoe.bin.bin_number }</td>
                            <td>
                                <button onClick={() => handleDelete(shoe.id)} className="btn btn-primary">Delete</button>
                            </td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    )
}

export default ShoesList
