import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesList from './ShoesList';
import ShoeForm from './ShoeForm';
import HatForm from './HatForm';
import HatList from './HatList';
import { useState, useEffect } from 'react';

function App(props) {
  const [hats, setHats] = useState([]);

  // This part will fetch the hats from the backend
  const fetchHats = async () => {
    const response = await fetch('http://localhost:8090/hats/'); 
    if (response.ok) {
      const data = await response.json(); 
      console.log(data)
      setHats(data.hats);
    }
  };

  // This thingy will run once when the component is mounted
  useEffect(() => {
    fetchHats();
  }, []);

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes">
            <Route path="" element={<ShoesList shoes={props.shoes} />} />
            <Route path="new" element={<ShoeForm />} />
          </Route>
          <Route path="hats">
            <Route index element={<HatList hats={hats} />} />
            <Route path="new" element={<HatForm fetchHats={fetchHats} />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
