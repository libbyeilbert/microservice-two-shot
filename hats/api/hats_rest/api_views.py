import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Hat, LocationVO 
from common.json import ModelEncoder


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ["import_href", "closet_name"]


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = ["fabric", "style_name", "color", "picture_url", "location", "id"]
    encoders = {"location": LocationVODetailEncoder()}



class HatDetailEncoder(ModelEncoder):       # this inherits from the modelencoder that helps with json 
    model = Hat                             # serialization of model instances.
    properties = ["fabric", "style_name", "color", "picture_url", "location"]
    encoders = {"location": LocationVODetailEncoder()}


@require_http_methods(["GET", "POST"])
def api_list_hat(request):
    if request.method == 'GET':
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content['location'] = location 
        except LocationVO.DoesNotExist:
            return JsonResponse({"message": "Invalid Location"}, status=400,)
        
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False
        )
    
        
@require_http_methods(["GET", "DELETE"])
def api_hat_detail(request, pk):
    try:
        hat = Hat.objects.get(id=pk)
    except Hat.DoesNotExist:
        return JsonResponse({"error": "Hat not found"}, status=404)
    
    if request.method == 'GET':
        return JsonResponse(HatDetailEncoder(hat).data, safe=False)
    elif request.method == 'DELETE':
        hat.delete()
        return JsonResponse({"success": "Hat deleted!"}, status=200)
