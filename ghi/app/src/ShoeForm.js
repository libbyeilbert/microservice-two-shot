import React, { useEffect, useState } from 'react';

function ShoeForm () {
    const [bins, setBins] = useState([]);
    const [manufacturer, setManufacturer] = useState('');
    const [model_name, setModelName] = useState('');
    const [color, setColor] = useState('');
    const [picture_url, setPictureUrl] = useState('');
    const [bin, setBin] = useState('');
    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    };
    const handleModelNameChange = (event) => {
        const value = event.target.value;
        setModelName(value);
    };
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    };
    const handlePictureUrlChange = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    };
    const handleBinChange = (event) => {
        const value = event.target.value;
        setBin(value);
    };
    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {}
        data.manufacturer = manufacturer;
        data.model_name = model_name;
        data.color = color;
        data.picture_url = picture_url;
        data.bin = bin;
        console.log(data);

        const shoeUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            header: {
                'Content-Type':'application/json',
            },
        };
        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
            const newShoe = await response.json();
            console.log(newShoe);
            setManufacturer('');
            setModelName('');
            setColor('');
            setPictureUrl('');
            setBin('');
        }
    };
    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setBins(data.bins);
        }
    };
    useEffect(() => {
        fetchData();
    }, []);
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new shoe</h1>
                    <form onSubmit={handleSubmit} id="create-shoe-form">
                    <label htmlFor="manufacturer">Manufacturer</label>

                        <div className="form-floating mb-3">
                            <input value={manufacturer} onChange={handleManufacturerChange} placeholder="Manufacturer" type="text" name="manufacturer" id="manufacturer" />
                        </div>
                        <label htmlFor="model_name" className="form-label">Model Name</label>
                        <div className="form-floating mb-3">


                            <input value={model_name} onChange={handleModelNameChange} placeholder="Model Name" type="text" name="model_name" id="model_name" />
                        </div>
                        <label htmlFor="color">Color</label>
                        <div className="form-floating mb-3">
                            <input value={color} onChange={handleColorChange} placeholder="Color" type="text" name="color" id="color" />
                        </div>
                        <label htmlFor="picture_url">Picture</label>
                        <div className="form-floating mb-3">

                            <input value={picture_url} onChange={handlePictureUrlChange}  placeholder="Picture" type="text" name="picture_url" id="picture_url" />
                        </div>
                        <div className="mb-3">
                            <select value={bin} onChange={handleBinChange} required name="bin" id="bin" className="form-select">
                                <option value="">Choose a bin</option>
                                {bins.map(bin => {
                                    return (
                                        <option key={bin.href} value={bin.href}>
                                            {bin.closet_name}, {bin.bin_number}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default ShoeForm;
