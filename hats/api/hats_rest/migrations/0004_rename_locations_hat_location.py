# Generated by Django 4.0.3 on 2023-07-20 23:30

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0003_rename_name_locationvo_closet_name'),
    ]

    operations = [
        migrations.RenameField(
            model_name='hat',
            old_name='locations',
            new_name='location',
        ),
    ]
